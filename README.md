# Utils

**Author: Amirreza Ashouri**
**Written in swift**

1. All utilities and code snipets i gathered from my projects in one module.
2. Easy to constraints views programatically using stack views.
3. Some handy and flexible initializers for customizing views.
4. Bindable implementation for MVVM-Bindable architecture.
