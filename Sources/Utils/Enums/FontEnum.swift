//
//  File.swift
//  
//
//  Created by amirreza on 4/18/21.
//

import UIKit

public enum FontsSize: CGFloat {
    case XSmall = 10
    case Small = 12
    case Medium = 14
    case Large = 16
    case XLarge = 18
    case XXLarge = 24
    case XXXLarge = 32
}
