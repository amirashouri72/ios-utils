//
//  File.swift
//  
//
//  Created by amirreza on 4/18/21.
//

import Foundation

public enum NetworkError: String, Error {
    case authLoginError = "something went wrong in login"
    case authSignUpError = "something went wrong in signup"
    case dataSerializationError = "something went wrong in getting information"
    case sotrageDownloadError = "something went wrong in downloading url in storing"
    case sotrageUploadError = "something went wrong uploading url in storing"
    case notAuthorized = "Your user is not authorized"
    case firestoreUpdatingDocumentError = "Failed to set the data for the given path"
    case sendingVerificationEmailError = "something went wrong in sending verification email to reset the password"
    case fireStoreGetDataError = "failed to fetch data from database"
    case coreDataCreateObjectError = "core data failed to create new object."
}

public enum AlertMessages: String {
    case alertProfileSaved = "پروفایل ذخیره شد"
    case alertErrorTitle = "Error"
    case alertOkText = "OK"
    case alertYesText = "YES"
    case alertNoText = "NO"
    
    case inValidEmail = "Please provide a valid email."
    case invalidFirstLetterCaps = "First Letter should be capital"
    case inValidPhone = "Invalid Phone"
    case invalidAlphabeticString = "Invalid String"
    case inValidPSW = "Invalid Password"
    case invalidPassword = "The password must be 6 characters long or more"
    case invalidNickName = "nickName should be between 3 to 12 letters long"
    
    case emptyPhone = "Empty Phone"
    case emptyEmail = "Please provide an email to continue."
    case emptyFirstLetterCaps = "Empty Name"
    case emptyAlphabeticString = "Empty String"
    case emptyPSW = "Empty Password"
    
    case invalidCoutry = "please select your country"
    case invalidState = "please select your State"
    case invalidCity = "please select your city"
    
    func localized() -> String {
        return NSLocalizedString(self.rawValue, comment: "")
    }
}

public struct ErrorModel: Error {
    public let error: NetworkError
    public let description: String
    
    public init(error: NetworkError, description: String) {
        self.error = error
        self.description = description
    }
}
