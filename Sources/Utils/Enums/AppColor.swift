//
//  File.swift
//  
//
//  Created by amirreza on 4/19/21.
//

import UIKit

public enum AppColorName: String {
    case background = "backgroundColor"
    case primary = "primaryColor"
    case secondary = "secondaryColor"
    case greyColor = "greyColor"
    case lightGreyColor = "lightGreyColor"
    case greenColor = "greenColor"
    case redColor = "redColor"
}

public func AppColor(in name: AppColorName) -> UIColor? {
    return UIColor(named: name.rawValue)
}
