//
//  File.swift
//  
//
//  Created by amirreza on 4/18/21.
//

import UIKit

public enum ConstraintConstants: CGFloat {
    case small = 8.0
    case medium = 16.0
    case large = 24.0
    case xLarge = 32.0
}
