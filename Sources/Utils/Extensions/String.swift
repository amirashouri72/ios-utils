//
//  File.swift
//  
//
//  Created by amirreza on 4/18/21.
//

import UIKit

public extension String {
    
    var containsEmoji: Bool { contains { $0.isEmoji } }
    
    func getAttributed(with color: UIColor, font: UIFont = appFont(isBold: true, size: .Medium)) -> NSAttributedString {
        let attrTitle = NSAttributedString(string: self, attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: color])
        return attrTitle
    }
    
    func justifyLabel() -> NSAttributedString {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.justified
        let attributedString = NSAttributedString(string: self,
                                                  attributes: [
                                                    NSAttributedString.Key.paragraphStyle: paragraphStyle,
                                                    NSAttributedString.Key.baselineOffset: NSNumber(value: 0)
        ])
        
        return attributedString
    }
    
    func generateQRCode() -> UIImage? {
        let data = self.data(using: String.Encoding.ascii)

        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 5, y: 5)

            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }

        return nil
    }
    
    func ReverseFormatAsPrice(charecters: [String] = [",", "$"]) -> String {
        var modifiedString = self
        charecters.forEach { char in
            modifiedString = modifiedString.replacingOccurrences(of: char, with: "")
        }
        return modifiedString
    }
    
    func formatAsPrice() -> String {
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.locale = NSLocale.init(localeIdentifier: "en_US") as Locale?
        formatter.maximumFractionDigits = 10
        
        let numberWithOutCommas = self.replacingOccurrences(of: ",", with: "")
        if let number = formatter.number(from: numberWithOutCommas) {
            let formattedString = formatter.string(from: number)
            return formattedString ?? self
        }
        
        return self
    }
    
    func currencyFormatting() -> String {
        
        if let value = Double(self.trim(with: ["$", ".", ","])) {
            let formatter = NumberFormatter()
            formatter.numberStyle = .currency
            formatter.maximumFractionDigits = 2
            formatter.minimumFractionDigits = 2
            if let str = formatter.string(for: value / 100) {
                return str
            }
        }
        return ""
    }
    
    func trim(with charecters: [String]) -> String {
        var trimmedString = self
        charecters.forEach { char in
            trimmedString = trimmedString.replacingOccurrences(of: char, with: "")
        }
        return trimmedString
    }
    
    func imageFromBase64() -> UIImage? {
        if let url = URL(string: self), let data = try? Data(contentsOf: url) {
            return UIImage(data: data)
        }
        return nil
    }
    
    func toDate(with format: String = "yyyy-MM-dd'T'HH:mm:ssZ") -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.date(from: self)
    }
}

public extension Array where Element: Hashable {
    func difference(from other: [Element]) -> [Element] {
        let thisSet = Set(self)
        let otherSet = Set(other)
        return Array(thisSet.symmetricDifference(otherSet))
    }
}
