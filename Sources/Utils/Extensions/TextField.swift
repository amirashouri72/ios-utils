//
//  File.swift
//  
//
//  Created by amirreza on 4/18/21.
//

import Foundation

import UIKit

public struct Border {
    let width: CGFloat
    let color: UIColor
    
    public init(width: CGFloat, color: UIColor) {
        self.width = width
        self.color = color
    }
}

public class CustomTextField: UITextField {
    public var textPadding = UIEdgeInsets(top: 0, left: padding(.medium), bottom: 0, right: 0)

    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: textPadding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: textPadding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: textPadding)
    }
}

public extension CustomTextField {
    
    convenience init(placeholderText: String, placeHolderTextColor: UIColor? = nil, textColor: UIColor = .lightGray, font: UIFont, align: NSTextAlignment, border: Border = Border(width: 0, color: .clear), keyboardtype: UIKeyboardType = .default, backGroundColor: UIColor = .white, cornerRadi: CGFloat = padding(.large), contentType: UITextContentType = .name, lock: Bool = false, hasShadow: Bool = false, shadowColor: UIColor = .black, textPadding: UIEdgeInsets? = nil) {
        self.init()
        self.font = font
        placeholder = placeholderText
        if let color = placeHolderTextColor {
            let attStr = NSAttributedString(string: placeholderText, attributes: [.foregroundColor: color])
            attributedPlaceholder = attStr
        }
        self.textColor = textColor
        textAlignment = align
        keyboardType = keyboardtype
        borderWidth = border.width
        borderColor = border.color
        backgroundColor = backGroundColor
        cornerRadius = cornerRadi
        if let pd = textPadding {
            self.textPadding = pd
        }
        textContentType = contentType
        adjustsFontSizeToFitWidth = true
        minimumFontSize = 10
        isUserInteractionEnabled = !lock
        autocorrectionType = .no
        if contentType == .password {
            isSecureTextEntry = true
        }
        if hasShadow {
            layer.shadowColor = shadowColor.cgColor
            layer.shadowRadius = 5
            layer.shadowOffset = CGSize.init(width: 1, height: 1)
            layer.shadowOpacity = 0.3
        }
    }
}

public extension UITextField {
    func bindText<Destination>(to destination: Destination, on property: WritableKeyPath<Destination, String?>) -> Binding<Destination> {
        return Binding(source: self, destination: destination, property: property)
    }
}
