//
//  File.swift
//  
//
//  Created by amirreza on 6/4/21.
//

import Foundation

public extension Double {
    func priceRepresentation() -> String {
        let price = String(self.magnitude).formatAsPrice()
        return "\(self >= 0 ? "" : "-")$" + price
    }
    
    func round(to places: Int = 2) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
