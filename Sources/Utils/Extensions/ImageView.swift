//
//  File.swift
//  
//
//  Created by amirreza on 4/18/21.
//

import Foundation

import UIKit

public extension UIImageView {
    
    convenience init(cornerRadius: CGFloat, contentMode: UIView.ContentMode = .scaleAspectFit, image: UIImage? = nil) {
        self.init(image: image)
        self.layer.cornerRadius = cornerRadius
        self.clipsToBounds = true
        self.contentMode = contentMode
    }
}

public extension UILabel {
    convenience init(text: String, font: UIFont, numberOfLines: Int = 1,textAlign: NSTextAlignment = .right,ishidden: Bool = false, Color: UIColor = .black) {
        self.init(frame: .zero)
        self.text = text
        self.font = font
        self.numberOfLines = numberOfLines
        isHidden = ishidden
        textColor = Color
        textAlignment = textAlign
        adjustsFontSizeToFitWidth = true
    }
}
