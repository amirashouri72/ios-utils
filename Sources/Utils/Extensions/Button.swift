//
//  File.swift
//  
//
//  Created by amirreza on 4/18/21.
//

import UIKit

public extension UIButton {
    convenience init(title: String, color: UIColor, textColor: UIColor, cornerRadious: CGFloat, font: UIFont, border: Border = .init(width: 0, color: .clear), isEnable: Bool = true, image: UIImage? = nil, imagePadding: UIEdgeInsets? = nil, titlePadding: UIEdgeInsets? = nil, isHidden: Bool = false, type: UIButton.ButtonType = .system, buttonTag: Int = 0) {
        self.init(type: type)
        let attributedTitle = title.getAttributed(with: textColor,font: font)
        setAttributedTitle(attributedTitle, for: .normal)
        self.setTitle(title, for: .normal)
        setTitleColor(textColor, for: .normal)
        backgroundColor = color
        layer.cornerRadius = cornerRadious
        tag = buttonTag
        self.borderWidth = border.width
        self.borderColor = border.color
        isEnabled = isEnable
        self.isHidden = isHidden
        if let img = image {
            setImage(img.withRenderingMode(.alwaysOriginal), for: .normal)
            //semanticContentAttribute = .forceRightToLeft
            if let edge = imagePadding {
                imageEdgeInsets = edge
                contentMode = .scaleAspectFit
            }
            if let edge = titlePadding {
                titleEdgeInsets = edge
                contentMode = .scaleAspectFit
            }
        }
    }
}

public  class CheckBox: UIButton {
    // Images
    let checkedImage = #imageLiteral(resourceName: "radioOn")
    let uncheckedImage = #imageLiteral(resourceName: "radioOff")
    
    // Bool property
    var isChecked: Bool = false {
        didSet {
            if isChecked == true {
                self.setImage(checkedImage, for: UIControl.State.normal)
            } else {
                self.setImage(uncheckedImage, for: UIControl.State.normal)
            }
        }
    }
}
