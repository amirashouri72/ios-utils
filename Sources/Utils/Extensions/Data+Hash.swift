//
//  File.swift
//  
//
//  Created by amirreza on 4/18/21.
//

import Foundation
import CryptoKit

public extension Data {
    
    func MD5() -> String {
        return Insecure.MD5.hash(data: self).map { String(format: "%02hhx", $0)}.joined()
    }
}
