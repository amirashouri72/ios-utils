//
//  File.swift
//  
//
//  Created by amirreza on 4/18/21.
//

import UIKit

public extension UIStackView {
    convenience init(_ views: [UIView], axis: NSLayoutConstraint.Axis, distribution: UIStackView.Distribution, alignment: UIStackView.Alignment, spacing: CGFloat, margin: UIEdgeInsets = .init(top: 0, left: 0, bottom: 0, right: 0)) {
        self.init(arrangedSubviews: views)
        self.axis = axis
        self.distribution = distribution
        self.spacing = spacing
        self.alignment = alignment
        layoutMargins = margin
        isLayoutMarginsRelativeArrangement = true
    }
}

public class CardViewStackView: UIView {
    
    var imageView: UIImageView?
    
    init(_ subViews: [UIView], axis: NSLayoutConstraint.Axis, distribution: UIStackView.Distribution, alignment: UIStackView.Alignment, spacing: CGFloat, cornerRadi:CGFloat, color: UIColor = .white,hasBorder: Bool = false, circleImageView: UIImageView? = nil, tagNumber: Int = 0, margin: UIEdgeInsets = .init(top: 0, left: 0, bottom: 0, right: 0), shadowColor: UIColor){
        super.init(frame: .zero)
        let vStack = UIStackView(subViews, axis: axis, distribution: distribution, alignment: alignment, spacing: spacing, margin: margin)
        backgroundColor = color
        layer.masksToBounds = false
        layer.shadowColor = shadowColor.cgColor
        layer.shadowOpacity = 0.8
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = cornerRadi / 3
        layer.cornerRadius = cornerRadi
        tag = tagNumber
        clipsToBounds = false
        
        addSubview(vStack)
        vStack.fillSuperview(padding: .init(top: 2 * padding(.xLarge), left: 0, bottom: 0, right: 0))
        
        let circleView = CardView(cornerRadi: 2 * padding(.xLarge), color: .clear)
        circleView.isUserInteractionEnabled = true
        
        addSubview(circleView)
        circleView.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: -2 * padding(.xLarge), left: 0, bottom: 0, right: 0),size: .init(width: 4 * padding(.xLarge), height: 4 * padding(.xLarge)))
        circleView.centerXInSuperview()
        
        if let iV = circleImageView {
            isUserInteractionEnabled = true
            imageView = iV
            circleView.addSubview(iV)
            iV.fillSuperview(padding: .init(top: 4, left: 4, bottom: 4, right: 4))
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
