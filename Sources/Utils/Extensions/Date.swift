//
//  File.swift
//  
//
//  Created by amirreza on 5/14/21.
//

import Foundation

public extension Date {
    func timeAgoDisplay() -> String {
        let formatter = RelativeDateTimeFormatter()
        formatter.unitsStyle = .full
        return formatter.localizedString(for: self, relativeTo: Date())
    }
    
    func toString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d - HH:mm"
        return formatter.string(from: self)
    }
}

