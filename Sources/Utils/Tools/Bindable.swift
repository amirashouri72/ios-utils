//
//  File.swift
//  
//
//  Created by amirreza on 4/18/21.
//

import Foundation

public class Bindable<T> {
    public var value: T? {
        didSet {
            observer?(value)
        }
    }
    
//    public init(_ value: T) {
//        self.value = value
//    }
    
    public init() {
    }
    
    public var observer: ((T?)->())?
    
    public func bind(observer: @escaping (T?) ->()) {
        self.observer = observer
    }
    
}
