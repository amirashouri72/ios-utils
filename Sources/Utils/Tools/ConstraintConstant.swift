//
//  File.swift
//  
//
//  Created by amirreza on 4/18/21.
//

import UIKit

public func padding(_ size: ConstraintConstants) -> CGFloat {
    switch size {
    case .large:
        return ConstraintConstants.large.rawValue
    case .small:
        return ConstraintConstants.small.rawValue
    case .medium:
        return ConstraintConstants.medium.rawValue
    case .xLarge:
        return ConstraintConstants.xLarge.rawValue
    }
}
