//
//  File.swift
//  
//
//  Created by amirreza on 4/18/21.
//

import Foundation

public enum Alert {
    case success
    case failure
    case error
}

public enum Valid {
    case success
    case failure(Alert, AlertMessages)
}
public enum ValidationType {
    case email
    case stringWithFirstLetterCaps
    case phoneNo
    case alphabeticString
    case password
}
public enum RegEx: String {
    case email = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}" // Email
    case password = "^.{6,15}$" // Password length 6-15
    case nickName = "^.{3,12}$" // nickName length 3-12
    case alphabeticStringWithSpace = "^[a-zA-Z ]*$" // e.g. hello sandeep
    case alphabeticStringFirstLetterCaps = "^[A-Z]+[a-zA-Z]*$" // SandsHell
    case phoneNo = "^0[0-9]{10}$" // PhoneNo 10-14 Digits        //Change RegEx according to your Requirement
    case cardNo = "[0-9]{16}"
    case mobileNo = "^.9[0-9]{9}|^.8[0-9]{10}"
    case landLine = ""
    case price = "[0-9]{1,}"
    case pin = "[0-9]{4,20}"
    case expireDate = "[0-9]{2}"
    case cvv2 = "[0-9]{3,4}"
    case billNo = "[0-9]{3,13}"
    case payNo = "[0-9]{1,13}"
}

public class Validation: NSObject {
    
    public static let shared = Validation()
    
    public func validate(values: (type: ValidationType, inputValue: String)...) -> Valid {
        for valueToBeChecked in values {
            switch valueToBeChecked.type {
            case .email:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .email, .emptyEmail, .inValidEmail)) {
                    return tempValue
                }
            case .stringWithFirstLetterCaps:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .alphabeticStringFirstLetterCaps, .emptyFirstLetterCaps, .invalidFirstLetterCaps)) {
                    return tempValue
                }
            case .phoneNo:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .phoneNo, .emptyPhone, .inValidPhone)) {
                    return tempValue
                }
            case .alphabeticString:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .alphabeticStringWithSpace, .emptyAlphabeticString, .invalidAlphabeticString)) {
                    return tempValue
                }
            case .password:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .password, .emptyPSW, .inValidPSW)) {
                    return tempValue
                }
            }
        }
        return .success
    }
    
    //func isValidStrings
    
    public func isValidString(_ input: (text: String, regex: RegEx, emptyAlert: AlertMessages, invalidAlert: AlertMessages)) -> Valid? {
        if input.text.isEmpty {
            return .failure(.error, input.emptyAlert)
        } else if isValidRegEx(input.text, input.regex) != true {
            return .failure(.error, input.invalidAlert)
        }
        return nil
    }
    
    public func isValidString(text: String, regex: RegEx, emptyAlert: AlertMessages, invalidAlert: AlertMessages) -> String? {
        if text.isEmpty {
            return emptyAlert.rawValue
        } else if isValidRegEx(text, regex) != true {
            return invalidAlert.rawValue
        }
        return nil
    }
    
    public func isValidString(_ array:[(String, RegEx, AlertMessages)]) -> String? {
        
        var errStr: String = ""
        
        array.forEach{ item in
            if !isValidRegEx(item.0, item.1){
                
                errStr += item.2 == array.last?.2 ? item.2.rawValue : item.2.rawValue + "\n"
            }
        }
        
        return errStr.isEmpty ? nil : errStr
    }
    
    public func isValidRegEx(_ testStr: String, _ regex: RegEx) -> Bool {
        let stringTest = NSPredicate(format:"SELF MATCHES %@", regex.rawValue)
        let result = stringTest.evaluate(with: testStr)
        return result
    }
}
