//
//  File.swift
//  
//
//  Created by amirreza on 5/10/21.
//

import Foundation

/// this protocol helps to discard all the boilerplate from the closure call sides
protocol Weakifiable: AnyObject { }

extension NSObject: Weakifiable { }

extension Weakifiable {
    func weakify<T>(_ code: @escaping (Self, T) -> Void) -> (T) -> Void {
        return {
            /* Begin boilerplate */
            [weak self] (data) in
            
            guard let self = self else { return }
            /* End boilerplate */
            
            code(self, data)
        }
    }
}
