//
//  File.swift
//  
//
//  Created by amirreza on 4/18/21.
//
import Foundation
import UIKit

public func appFont(isBold: Bool, size: FontsSize) -> UIFont {
   
   let fontName = isBold ? "Roboto-Bold" : "Roboto-Regular"
   
   return UIFont(name: fontName, size: size.rawValue) ?? UIFont()
   
}

public func registerFonts() {
    _ = UIFont.registerFont(bundle: .module, fontName: "Roboto-Bold", fontExtension: "ttf")
    _ = UIFont.registerFont(bundle: .module, fontName: "Roboto-Regular", fontExtension: "ttf")
}

extension UIFont {
    static func registerFont(bundle: Bundle, fontName: String, fontExtension: String) -> Bool {

        guard let fontURL = bundle.url(forResource: fontName, withExtension: fontExtension) else {
            fatalError("Couldn't find font \(fontName)")
        }

        guard let fontDataProvider = CGDataProvider(url: fontURL as CFURL) else {
            fatalError("Couldn't load data from the font \(fontName)")
        }

        guard let font = CGFont(fontDataProvider) else {
            fatalError("Couldn't create font from data")
        }

        var error: Unmanaged<CFError>?
        let success = CTFontManagerRegisterGraphicsFont(font, &error)
        guard success else {
            print("Error registering font: maybe it was already registered.")
            return false
        }

        return true
    }
}
