//
//  File.swift
//  
//
//  Created by amirreza on 5/10/21.
//

import UIKit

/// this class helps to use objective-c methods in a cleaner swifty way.
public class Binding<Destination: AnyObject>: NSObject {
    weak var source: UITextField?
    weak var destination: Destination?
    var property: WritableKeyPath<Destination, String?>
    
    init(source: UITextField,
         destination: Destination,
         property: WritableKeyPath<Destination, String?>) {
        self.source = source
        self.destination = destination
        self.property = property
        
        super.init()
        
        self.source?.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    }
    
    @objc func textFieldDidChange() {
        destination?[keyPath: property] = source?.text
    }
}
